package com.KawakawaPlanning.djgotokki_server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.TitleEvent;
import org.eclipse.swt.browser.TitleListener;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.scene.web.WebView;

public class Video {
    static WebView webview;
    static boolean playing = false;
	static ArrayList<String> stockId = new ArrayList<String>();
	static ArrayList<String> stockTi = new ArrayList<String>();
	public static Browser browser;
	private static Label statusLabel;
	static Main main;
	static Display disp;
	static List list2;
	static int nowNum = 0;
	static boolean portE=false;
	static Thread thread;
	static MessageBox box;
	static MessageBox boxok;
	static boolean adminEn = false;
	static String adminIp;
	boolean first = true ;
	static String mServerId;
	 
	public Video(String str) {
		mServerId = str;
		System.out.println(str);
		/* 別スレッドとして動作させるオブジェクトを作成 */
		SubThread sub = new SubThread();

	    /* 別のスレッドを作成し、スレッドを開始する */
	    thread = new Thread(sub);
	    thread.start();
	    
	    disp = new Display();
	    
	    final Shell shell = new Shell(disp);
        shell.setText("DJ GOTOKKI Client");
        shell.setLayout(new GridLayout(4, false));
        
        shell.addShellListener(new ShellAdapter(){
            public void shellClosed(ShellEvent e) {
                MessageBox msg = new MessageBox(shell ,SWT.OK |SWT.CANCEL |SWT.ICON_WARNING);
                msg.setText("本当に終了しますか?");
                msg.setMessage("本当に終了しますか?");
                int ret = msg.open();
                if (ret == SWT.CANCEL){
                    e.doit = false;
                    return;
                }else{
                	System.exit(0);
                }
            }
        });
        
        
        Menu menubar = new Menu(shell,SWT.BAR);
        shell.setMenuBar(menubar);
        
        MenuItem item1 = new MenuItem(menubar,SWT.CASCADE);
        item1.setText("メニュー");
        
        Menu menu1 = new Menu(item1);
        item1.setMenu(menu1);
        
        MenuItem item1_1 = new MenuItem(menu1,SWT.PUSH);
        item1_1.setText("このソフトウェアについて");
        
        new MenuItem(menu1,SWT.SEPARATOR);
        MenuItem item1_3 = new MenuItem(menu1,SWT.PUSH);
        item1_3.setText("終了");
        item1_3.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
                MessageBox msg = new MessageBox(shell,SWT.OK |SWT.CANCEL |SWT.ICON_WARNING);
                msg.setText("本当に終了しますか?");
                msg.setMessage("本当に終了しますか?");
                int ret = msg.open();
                if (ret==SWT.OK){
                	System.exit(0);
                }
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
        
        box = new MessageBox(shell,SWT.YES|SWT.NO);
        boxok = new MessageBox(shell,SWT.OK);
        
        
        
        GridData gd;
        
        list2 = new List(shell,SWT.SINGLE|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
        list2.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent event) {
            	try{
            		int[] selectedItems = list2.getSelectionIndices();
            		String outString = "";
              	    for (int loopIndex = 0; loopIndex < selectedItems.length; loopIndex++)
            	  	outString = selectedItems[loopIndex] + "";
              		System.out.println(outString);
              		nowNum = Integer.parseInt(outString);
            	}catch(Exception e){
            	}
            }

            public void widgetDefaultSelected(SelectionEvent event) {}
          });
        gd = new GridData(GridData.VERTICAL_ALIGN_FILL|GridData.HORIZONTAL_ALIGN_FILL);
        gd.horizontalSpan = 1;
        gd.horizontalIndent = 10;
        gd.verticalIndent = 10;
        list2.setLayoutData(gd);
        
        browser = new Browser(shell, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 3;
        browser.setLayoutData(gd);


		
        statusLabel = new Label(shell, SWT.NONE);
        gd = new GridData(GridData.FILL);
        gd.horizontalSpan = 1;
        statusLabel.setLayoutData(gd);          
        if(portE){
        	statusLabel.setText("ポートエラー／ポート設定をしてください。");
        }else{
        	statusLabel.setText("未再生／曲送信受付中。");
        }


		gd = new GridData(GridData.FILL);
		gd.horizontalSpan = 3;
		Label label1 = new Label(shell,SWT.NULL);
		label1.setText("サーバーID:" + mServerId);
		label1.setLayoutData(gd);
        
        Button delButton = new Button(shell, SWT.PUSH);
        delButton.setText("選択している曲を削除する");
        gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        delButton.setLayoutData(gd);
        delButton.addSelectionListener(new SelectionAdapter(){
                public void widgetSelected(SelectionEvent e) {
                	del();
                }

        });
        
        Button jumpButton = new Button(shell, SWT.PUSH);
        jumpButton.setText("この曲をスキップする");
        gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        jumpButton.setLayoutData(gd);
        jumpButton.addSelectionListener(new SelectionAdapter(){
                public void widgetSelected(SelectionEvent e) {
                    next();
                }

        });

        
        shell.setText("DJ GOTOKKI Server");
        
        browser.addTitleListener(new TitleListener() {
			
			@Override
			public void changed(TitleEvent arg0) {
				if(arg0.title.equals("next1") || arg0.title.equals("next2")){
					next();
				}
				
			}
		});
        browser.setUrl("http://kawakawaplanning.github.io/js_test.html");
        shell.setSize(800,600);
        shell.addControlListener(new ControlListener() {
        	  public void controlMoved(ControlEvent e){
        	  }
        	  public void controlResized(ControlEvent e){
        	    shell.setSize(800,600);
        	  }
        	});
        shell.open();
        
        while (!shell.isDisposed()) {
                if (!disp.readAndDispatch()) {
                        disp.sleep();
                }
        }
        disp.dispose();
	}


	static void first() {
    	    disp.syncExec(new Runnable() {
     	         public void run() {
     	        	browser.execute("playVideo(\""+stockId.get(0)+"\")");
     	        	nowNum = 0;
     	         }
     	    });
    	    disp.syncExec(new Runnable() {
    	         public void run() {
    	        	 statusLabel.setText("再生中／曲送信受付中。");
    	         }
    	    });
			
	}
	static void next() {
		if (playing == true){
			
		playing = false;
		if(stockId.size() > 1){
    	    disp.syncExec(new Runnable() {
     	         public void run() {
     	        	
     	        	list2.remove(0);
     	        	stockTi.remove(0);
     	        	stockId.remove(0);
     	        	list2.select(0);
     	        	nowNum = 0;
     	        	browser.execute("playVideo(\""+stockId.get(0)+"\")");
     	         }
     	    });
			playing = true;
			
			disp.syncExec(new Runnable() {
   	         public void run() {
   	        	 statusLabel.setText("再生中／曲送信受付中。");
   	         }
   	    });
		}else{
 	        	
			disp.syncExec(new Runnable() {
   	         public void run() {
   	        	 list2.remove(0);
   	        	 stockTi.remove(0);
   	        	 stockId.remove(0);
   	        	 Video.playing = false;
   	        	 browser.execute("stopedVideo();");
   	        	 statusLabel.setText("未再生／曲を送信してください。");
   	         }
   	    });
		}
		}
	}
	static void del() {
    	disp.syncExec(new Runnable() {
     	     public void run() {
     	        	if(!SubThread.flag){
                		if(nowNum != 0){
                			System.out.println(stockTi.get(nowNum));
                			list2.remove(nowNum);
                			stockTi.remove(nowNum);
                			stockId.remove(nowNum);
                			nowNum = 0;
                			list2.select(0);
                		}
     	        	}
     	         }
     	    });
	}
	
}
class SubThread implements Runnable{
	static boolean flag = true;
	public void run(){
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				check();
			}
		},0,1000);
	}
	
	public static void check(){
		
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		HttpConnector httpConnector = new HttpConnector("get","{\"server_id\":\"" + Video.mServerId + "\"}");
		httpConnector.setOnHttpResponseListener(new OnHttpResponseListener() {
			
			@Override
			public void onResponse(final String response) {
				if(!response.equals("1")){
					System.out.println(response);
					Thread th = new Thread(new Runnable() {
						@Override
						public void run() {
							
				            if(flag){
				            	flag = false;
				            	Video.disp.asyncExec(new Runnable() {
				          	         public void run() {
				          	        	 Video.list2.removeAll();
				          	        	 Video.list2.select(0);
				          	        	 Video.nowNum = 0;
				          	         }
				          	    });
				            }
				            String[] video = response.split(",");
				            for (final String id:video){
				            
				            	try {
				            		
									String url = "https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyBEdFSE1PClEDQ2AnvQJ-SGe5QM9VIXJBQ";
						            url += "&id=" + id;
						            HttpUriRequest httpGet = new HttpGet(url);
						            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
						            HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
						            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		
						                try {
						                    JSONObject json = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
						                    JSONArray items = json.getJSONArray("items");
		
						                    final JSONObject ob2 = items.getJSONObject(0);
				      	      	      			Video.disp.syncExec(new Runnable() {
				      	      	      				public void run() {
				      	      	      					try {
				      								    	Video.list2.add(ob2.getJSONObject("snippet").get("title").toString());
				    										Video.stockTi.add(ob2.getJSONObject("snippet").get("title").toString());
				    										Video.stockId.add(id);	
				      	      	      					} catch (JSONException e) {
				      	      	      						System.out.println(e.toString());
				      	      	      					}
				      	      	      				}
				      	      	      			});
		
						                } catch (JSONException e) {
						                    e.printStackTrace();
						                }
						            }
				            		//ポート再設定時のステータス変更
									if(Video.playing == false){
										Video.playing = true;
										Video.first();
									}
								} catch (NumberFormatException e) {
									// TODO Auto-generated catch block
									System.out.println(e);
								} catch (ClientProtocolException e1) {
									// TODO 自動生成された catch ブロック
									e1.printStackTrace();
								} catch (IOException e1) {
									// TODO 自動生成された catch ブロック
									e1.printStackTrace();
								}
				            }
							}
						});
		            th.start();
				}
				
			}
		});
		httpConnector.post();
		
//		serverSocket = null;
//		try {
//            serverSocket = new ServerSocket(Video.PORT);
//            Video.portE=false;
//            System.out.println("start wait...");
//            // 接続があるまでブロック
//            socket = serverSocket.accept();
//            BufferedReader br =
//            		new BufferedReader(
//	            				new InputStreamReader(socket.getInputStream()));
//            final String str = br.readLine();
//            System.out.println(str);
//            
//            final String stri[] = str.split(",");
//            
//            if (stri[0].equals("getAdmin")){
//            	
////            	Video.next();
//            	Video.disp.asyncExec(new Runnable() {
//            		public void run() {
//            			if(!Video.adminEn){
//            				Video.box.setMessage("'"+stri[1]+"'がAdminister権限を要求してきました。\n要求を許可すると、その端末でサーバーの操作ができるようになります。\n許可しますか？");
//         	        		Video.box.setText("管理者権限の要求");
//         	        		int result = Video.box.open();
//         	        		if(result==SWT.YES){
//         	        			Video.adminEn = true;
//         	        	    	Video.adminIp = stri[1];
//         	        		}
//            			}
//         	         }
//         	    });
//            	
//            }else if(stri[0].equals("skip")){
//            	if(Video.adminEn && stri[1].equals(Video.adminIp)){
//            		Video.next();
//            	}
//            }else if(stri[0].equals("end")){
//            	
//            	PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
//                pw.println("end");
//                
//            }else if(stri[0].equals("reload")){
//            	if(Video.adminEn && stri[1].equals(Video.adminIp)){
//            		sendList(Video.adminIp, "12000");
//            	}
//            }else if(stri[0].equals("<policy-file-request/>\0")){
////            	if(Video.adminEn && stri[1].equals(Video.adminIp)){
//            		sendPoricy("192.168.43.8", "10000");
////            	}
//                    
//            }else{
//            	
//            Thread th = new Thread(new Runnable() {
//				@Override
//				public void run() {
//					
//		            if(flag){
//		            	flag = false;
//		            	Video.disp.asyncExec(new Runnable() {
//		          	         public void run() {
//		          	        	 Video.list2.removeAll();
//		          	        	 Video.list2.select(0);
//		          	        	 Video.nowNum = 0;
//		          	         }
//		          	    });
//		            }
//		            	
//		            	try {
//							
////							Video.display.syncExec(new Runnable() {
////								public void run() {
////							    	Video.list2.add(stri[1]);
////										Video.stockTi.add(stri[1]);
////						     	    Video.stockId.add(stri[0]);
////							    }
////							});
//		            		
//		            		
//							String url = "https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyBEdFSE1PClEDQ2AnvQJ-SGe5QM9VIXJBQ";
//				            url += "&id=" + stri[0];
//				            HttpUriRequest httpGet = new HttpGet(url);
//				            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
//				            HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
//				            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//
//				                try {
//				                    JSONObject json = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
//				                    JSONArray items = json.getJSONArray("items");
//
//				                    final JSONObject ob2 = items.getJSONObject(0);
//		      	      	      			Video.disp.syncExec(new Runnable() {
//		      	      	      				public void run() {
//		      	      	      					try {
//		      								    	Video.list2.add(ob2.getJSONObject("snippet").get("title").toString());
//		    										Video.stockTi.add(ob2.getJSONObject("snippet").get("title").toString());
//		    										Video.stockId.add(stri[0]);	
//		      	      	      					} catch (JSONException e) {
//		      	      	      						System.out.println(e.toString());
//		      	      	      					}
//		      	      	      				}
//		      	      	      			});
//
//				                } catch (JSONException e) {
//				                    e.printStackTrace();
//				                }
//				            }
//		            		//ポート再設定時のステータス変更
//							if(Video.playing == false){
//								Video.first();
//							}
//						} catch (NumberFormatException e) {
//							// TODO Auto-generated catch block
//							System.out.println(e);
//						} catch (ClientProtocolException e1) {
//							// TODO 自動生成された catch ブロック
//							e1.printStackTrace();
//						} catch (IOException e1) {
//							// TODO 自動生成された catch ブロック
//							e1.printStackTrace();
//						}
//		            	
//					}
//				});
//            th.start();
//            }
//                socket.close();
//                socket = null;
//                serverSocket.close();
//                serverSocket = null;
//            check();
//        } catch (IOException e) {
//        	System.out.println(e);
//            Video.portE=true;
//        }
		
        
	}
	public static void sendList(String HOST,String PORT){
		System.out.println("ktkr");
		Socket socket = null;
		int port = Integer.parseInt(PORT);
        try {
            socket = new Socket(HOST,port);
            PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
            for(int i = 0;i <= Video.stockTi.size()-1;i++){
            	pw.println( Video.stockTi.get(i)+"");
            }
 
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
 
        if( socket != null){
            try {
                socket.close();
                socket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	
	}
}