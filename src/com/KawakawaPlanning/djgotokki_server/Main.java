//Charset:UTF-8

package com.KawakawaPlanning.djgotokki_server;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class Main{

    static Main main;
    static Display display;
    static GridData gd;

    static Label label1;
    static Label label2;
    static Label label3;
    static Text text1;
    static Text text2;
    static Button loginBtn;
    static Button registBtn;
    static MessageBox msg1;

    public static void main(String[] args) { main = new Main(); }

    public Main() {
        display = new Display();
        
        final Shell sh = new Shell(display);
        sh.setText("ログイン");
        sh.setLayout(new GridLayout(1, false));

        msg1 = new MessageBox(sh,SWT.OK |SWT.CANCEL |SWT.ICON_WARNING);


        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.verticalSpan = 2;
        label1 = new Label(sh, SWT.CENTER);
        label1.setLayoutData(gd);
        label1.setText("G-JUKE SYSYTEM SERVER");
//        label1.setFont(new Font(display,"",20,SWT.NORMAL));

        gd.verticalSpan = 1;
        label2 = new Label(sh, SWT.CENTER);
        label2.setLayoutData(gd);
        label2.setText("ようこそ！ログインしてください。");

        label3 = new Label(sh, SWT.CENTER);
        label3.setLayoutData(gd);
        label3.setText("サーバーID");


        text1 = new Text(sh,SWT.SINGLE|SWT.BORDER);
        GridData gridData1 = new GridData();
        gridData1.horizontalAlignment = GridData.FILL;
        gridData1.grabExcessHorizontalSpace = true;
        text1.setLayoutData(gridData1);
        text1.addListener(SWT.Traverse, new Listener() {
            @Override
            public void handleEvent(Event event) {
                if(event.detail == SWT.TRAVERSE_RETURN) {
                    text2.setFocus();
                }
            }
        });


        Label label4 = new Label(sh, SWT.CENTER);
        label4.setLayoutData(gd);
        label4.setText("パスワード");

        text2 = new Text(sh,SWT.SINGLE|SWT.BORDER);
        text2.setLayoutData(gridData1);
        text2.addListener(SWT.Traverse, new Listener() {
            @Override
            public void handleEvent(Event event) {
                if(event.detail == SWT.TRAVERSE_RETURN) {
                    login();
                }
            }
        });

        loginBtn = new Button(sh, SWT.PUSH );
        loginBtn.setText("ログイン");
        loginBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
                login();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
        gd = new GridData(GridData.FILL_HORIZONTAL);
        loginBtn.setLayoutData(gd);

        registBtn = new Button(sh, SWT.PUSH );
        registBtn.setText("サーバー登録");
        registBtn.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                regist();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {}
        });
        registBtn.setLayoutData(gd);

        sh.addShellListener(new ShellAdapter(){
            public void shellClosed(ShellEvent e) {
                System.exit(0);
            }
        });
        
        sh.setSize(400,300);
        sh.addControlListener(new ControlListener() {
        	  public void controlMoved(ControlEvent e){
        	  }
        	  public void controlResized(ControlEvent e){
        	    sh.setSize(400,300);
        	  }
        	});
        sh.open();

        while (!sh.isDisposed()) {
                if (!display.readAndDispatch()) {
                        display.sleep();
                }
        }
        display.dispose();

    }

    public static void login() {

        HttpConnector httpConnector = new HttpConnector("login","{\"server_id\":\"" + text1.getText() + "\",\"password\":\"" + text2.getText() + "\"}");
        httpConnector.setOnHttpResponseListener(new OnHttpResponseListener() {

            @Override
            public void onResponse(final String response) {
                if(response.equals("0")){
                    display.syncExec(new Runnable() {
                        public void run() {
                            String str = text1.getText();
                            display.dispose();
                            new Video(str);
                        }
                    });

                }else{
                    display.syncExec(new Runnable() {
                        public void run() {
                            msg1.setText("エラー");
                            msg1.setMessage("ログインできませんでした。IDまたはPWを間違えていませんか？");
                            msg1.open();
                        }
                    });
                }
            }
        });
        httpConnector.post();

    }
    public static void regist() {
        if(!text1.getText().equals("") && !text2.getText().equals("")){
            HttpConnector httpConnector = new HttpConnector("regist","{\"server_id\":\"" + text1.getText() + "\",\"password\":\"" + text2.getText() + "\"}");
            httpConnector.setOnHttpResponseListener(new OnHttpResponseListener() {

                @Override
                public void onResponse(final String response) {
                    if(response.equals("0")){
                        display.syncExec(new Runnable() {
                            public void run() {
                                msg1.setText("登録完了");
                                msg1.setMessage("登録が完了しました。ログインボタンを押してログインしてください。");
                                msg1.open();
                            }
                        });
                    }else{
                        display.syncExec(new Runnable() {
                            public void run() {
                                msg1.setText("登録失敗");
                                msg1.setMessage("登録が失敗しました。すでにこのIDは使われています。IDを変えてお試しください。");
                                msg1.open();
                            }
                        });
                    }
                }
            });
            httpConnector.post();
        }else{
            msg1.setText("エラー");
            msg1.setMessage("登録したいIDとPWを入力してください。");
            msg1.open();
        }

    }
}
